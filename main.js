const electron = require('electron')

var {app, BrowserWindow, ipcMain} = electron; 
var fs = require('fs');
const dialog = require('electron').dialog;

const path = require('path')
const url = require('url')

let mainWindow

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 800, 
    height: 600,
    icon: path.join(__dirname, 'icon.png')
  })
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  //mainWindow.setMenu(null)

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', function(){
  createWindow();
  /*
  ipcMain.on('save', (event, arg) => {  
    dialog.showSaveDialog({ filters: [
       { name: 'JavaScript', extensions: ['js'] }

      ]}, function (fileName) {

      if (fileName === undefined) return;

      fs.writeFile(fileName, arg, function (err) {   

      });

    });
  });*/
  
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})
