//CODE IMPORTS
window.$ = window.jQuery = require('./jquery.js');

//JQUERY PLUGINS
require('./jquery-ui.min.js');

const dialog = require('electron').remote.dialog;
const app = require('electron').remote.app;
const window2 = require('electron').remote.getCurrentWindow();
var fs = require('fs');
var path = require('path');
const opn = require('opn');
  const shell = require('electron').shell;

//LOAD THEMES
var themeDir = path.join(__dirname, path.normalize("codemirror/theme/"));
fs.readdir(themeDir, (err, files) => {
  if (err) { console.log(err); }
  else{
    files.forEach(file => {
      var newPath = path.join(themeDir, file);
      loadjscssfile(newPath, "css");
    });
  }
});

//ADDITIONAL THEMES (disabled because they break overwrites)
/*
fs.readdir("themes/", (err, files) => {
  files.forEach(file => {
    loadjscssfile("themes/" + file, "css");
  });
});*/


//ARRAYS
var tabs = [];
var editors = [];

//VARIABLES
var lastActive;
var currentTheme = "mbo";

//TEMPORARY
var dirs = [];

//PLUGIN CALLS
$("#tabs").sortable();

//GLOBAL EDITOR CONFIGURATION
var options = {
  lineNumbers: true,
  theme: currentTheme,
  lineWrapping: true,
  autoCloseBrackets: true,
  matchBrackets: true,
  extraKeys: {
    "Ctrl-G": function(cm) {
      if (!$("#console").is(":visible")) $("#console").fadeIn();
      $("#console-input").focus();
    },
    "Ctrl-Q": function(cm) { app.quit(); },
    "Ctrl-N": function(cm) { newFile(); },
    "Ctrl-W": function(cm) { closeFile(); },
    "Ctrl-Tab": function(cm) {
      var currentIndex = tabs.indexOf($(".active").data("target"));
      var newIndex = currentIndex+1;
      if (currentIndex+1==tabs.length) newIndex=0;
      selectTab(tabs[newIndex]);
    },
    "Shift-Ctrl-Tab": function(cm) {
      var currentIndex = tabs.indexOf($(".active").data("target"));
      var newIndex = currentIndex-1;
      if (currentIndex==0) newIndex=tabs.length-1;
      selectTab(tabs[newIndex]);
    },
    "Shift-Ctrl-I": function(cm) { window2.toggleDevTools(); },
    "Ctrl-R": function(cm) { window2.reload(); },
    "Ctrl-O": function(cm) { openFile(); },
    "Ctrl-S": function(cm) { saveFile(); },
    "Shift-Ctrl-S": function(cm) { saveFileAs(); }
  }
};

//CREATE NEW UNTITLED TAB ON LAUNCH
newTab("untitled","");

//FUNCTONS
function newTab(name, data){
  var n = path.basename(name);
  if (name.includes("/")||name.includes("\\")) dirs.push(path.dirname(name));
  else dirs.push("none");
  //create tab id
  var tabId = makeid();
  //add tab to the list
  tabs.push(tabId);
  //deactivate previous tab
  $(".active").removeClass("active");
  //append the tab to other tabs
  $("#tabs").append(`
    <div class="tab active" data-target="${tabId}">
      <span class="name">${n}</span>
      <span class="close" data-parent="${tabId}">X</span>
    </div>`);
  //hide currently showing tab body
  $(".showing").hide();
  $(".showing").removeClass("showing");
  //create a new one
  $("#editor").append(`<div id="${tabId}" class="tabcontent showing"></div>`);
  //add event listener
  $(".active").click(function(){
    selectTab($(this).data("target"));
    //focus corresponding editor
    editors[tabs.indexOf($(this).data("target"))].focus();
  });
  //append close function
  $("span[data-parent='" + tabId +"']").click(function(){ quitTab($(this).data("parent")); });
  //add editor
  editors.push(CodeMirror(document.getElementById(tabId), options));
  //add code if data is set
  if (data!=""){ editors[editors.length - 1].setValue(data); }
  //focus current editor
  editors[editors.length - 1].focus();
  //update theme
  editors[editors.length - 1].setOption("theme", currentTheme);
  //select automatic file type
  var ftp = getLang(getExt(name));
  editors[editors.length - 1].setOption("mode", ftp);
  $("#language").val(ftp);
  //remove previous tab if it is untitled and empty (breaks creating more than 1 untitled tab)
  /*
  var tab = tabs[tabs.indexOf($(".active").data("target"))-1];
  if ($('#' + tab).val()==""&&editors[tabs.indexOf(tab)].getValue()=="") {
    quitTab(tab);
  }*/
}
function quitTab(idCode){
  //remove tab body
  $("#" + idCode).remove();
  //get index of current tab
  var tabIndex = tabs.indexOf(idCode);
  //if tab is active and isn't the last one select another tab
  if (tabs[tabIndex]==$(".active").data("target") && tabs.length>1) {
    if (tabIndex==0) { selectTab(tabs[tabIndex+1]); }
    else{ selectTab(tabs[tabIndex-1]); }
  }
  //remove tab from page
  $("div[data-target='" + idCode +"']").remove();
  //remove tab from array
  tabs.splice(tabIndex, 1);
  //remove dir form array
  dirs.splice(tabIndex, 1);
  //remove tab from editor
  editors.splice(tabIndex, 1);
  //if last tab closed, quit the app
  if (tabs.length==0){ app.quit(); }
}
function selectTab(tab){
  //hide editor
  $(".showing").hide();
  $(".showing").removeClass("showing");
  //hide old tab
  $(".active").removeClass("active");
  //show new tab
  $("div[data-target='" + tab +"']").addClass("active");
  $("#" + tab).show();
  $("#" + tab).addClass("showing");
  //focus corresponding editor
  editors[tabs.indexOf(tab)].focus();
  //set mode select value
  $("#language").val(editors[tabs.indexOf(tab)].getOption("mode"));
  if (dirs[tabs.indexOf(tab)]!="none") {
    document.title = dirs[tabs.indexOf(tab)] + "/" + $('.active > .name').text() + " | PTE v0.0.1";
  }
}
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 5; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}
function renameTab(name, tab){ $("div[data-target='" + tab +"'] > .name").text(name); }
function notify(text, fIn, fOut, life){
  //set notification text
  $(".notif").text(text);
  //fade in notification, then wait, then fade it out
  $(".notif").fadeIn(fIn, function(){ setTimeout(function(){ $(".notif").fadeOut(fOut); }, life); });
}
function getExt(fileName){
  var name = fileName.lastIndexOf('.');
  return fileName.substring(name + 1);
}
function getLang(fileName){
  switch (fileName){
    case "js": return "javascript"; break;
    case "html": return "htmlmixed"; break;
    case "css": return "css"; break;
    case "py": return "python"; break;
    case "php": return "php"; break;
    default: return "null";
  }
  /*
  if (fileName=="js") { return "javascript"; }
  else if (fileName=="html") { return "htmlmixed"; }
  else if (fileName=="css") { return "css"; }
  else if (fileName=="py") { return "python"; }
  else if (fileName=="php") { return "php"; }
  else { return "null"; }
  */
}
function getOpt(ext){
  var ind;
  var t_options = s_options;
  for (var i = 0; i < t_options.length; i++) if (t_options[i].extensions[0]==ext) ind = i;
  var temp = t_options[ind];
  t_options[ind] = t_options[0];
  t_options[0] = temp;
  return t_options;
}
function loadjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined") document.getElementsByTagName("head")[0].appendChild(fileref);
}
function newFile(){
  newTab("untitled","");
  document.title = "untitled" + " | PTE v0.0.1";
}
function openFile(){
  dialog.showOpenDialog({ filters: [
        { name: 'All files', extensions: ['*'] }
    ]}, function (fileNames) {
    if (fileNames === undefined) return;
    var fileName = fileNames[0];
    fs.readFile(fileName, 'utf-8', function (err, data) {
      if (err) { return; }
      newTab(fileName, data);
      document.title = fileName + " | PTE v0.0.1";
    });
  }); 
}
function saveFile(){
  var directory = dirs[tabs.indexOf($(".active").data("target"))];
  var fileName = directory + "/" + $('.active > .name').text();
  
  if (directory!="none" && fs.existsSync(fileName)) {
    var res = editors[tabs.indexOf($(".active").data("target"))].getValue();
    fs.writeFile(fileName, res, function (err) {
      if (err) return;
      renameTab(path.basename(fileName), $(".active").data("target"));
      notify("File saved!", 500, 500, 2000);
    });
  }
  else{  
    dialog.showSaveDialog({ filters: [
        { name: 'JavaScript', extensions: ['js'] },
        { name: 'Python', extensions: ['py'] },
        { name: 'HTML', extensions: ['html'] },
        { name: 'CSS', extensions: ['css'] },
        { name: 'PHP', extensions: ['php'] },
        { name: 'Text files', extensions: ['txt'] },
        { name: 'Any files', extensions: ['*'] }
      ]}, function (fileName) {
      if (fileName === undefined) return;
      var res = editors[tabs.indexOf($(".active").data("target"))].getValue();
      fs.writeFile(fileName, res, function (err) {
        if (err) return;
        var n = path.basename(fileName);
        renameTab(n, $(".active").data("target"));

        dirs[tabs.indexOf($(".active").data("target"))] = path.dirname(fileName);
        document.title = fileName + " | PTE v0.0.1";
        notify("File saved!", 500, 500, 2000);
      });
    });
  }
}
function saveFileAs(){
  dialog.showSaveDialog({ filters: [
      { name: 'JavaScript', extensions: ['js'] },
      { name: 'Python', extensions: ['py'] },
      { name: 'HTML', extensions: ['html'] },
      { name: 'CSS', extensions: ['css'] },
        { name: 'PHP', extensions: ['php'] },
        { name: 'Text files', extensions: ['txt'] },
        { name: 'Any files', extensions: ['*'] }
    ]}, function (fileName) {
    if (fileName === undefined) return;
    var res = editors[tabs.indexOf($(".active").data("target"))].getValue();
    fs.writeFile(fileName, res, function (err) {
      if (err) return;
      var n = path.basename(fileName);
      renameTab(n, $(".active").data("target"));
      notify("File saved!", 500, 500, 2000);
    });
  });
}
function closeFile(){ quitTab($(".active").data("target")); }
function runFile(app){
  var directory2 = dirs[tabs.indexOf($(".active").data("target"))];
  if (directory2 != "none") {
    var fileName2 = directory2 + "/" + $('.active > .name').text();
    //opn(fileName2, {app: 'firefox'});
    opn(fileName2);
  }
  else notify("Missing directory", 500, 500, 2000);
}
//breaks editor focus click
function setTheme(theme){
  currentTheme = theme;
  for (var i = 0; i < editors.length; i++) editors[i].setOption("theme", theme);
}

//EVENTS
$("#language").change(function(){
  //get index number of the editor to change
  var editorIndex = tabs.indexOf($(".active").data("target"));
  console.log("Setting mode to " + $("#language").val());
  //set editor mode to selected language
  editors[editorIndex].setOption("mode", $("#language").val());
  //force refresh editor just in case
  editors[editorIndex].refresh();
});

//TEMPORARY CONSOLE STUFF
$("#close-console").click(function(){
    $("#console").fadeOut();
    $("#console-text").text("");
    editors[tabs.indexOf($(".active").data("target"))].focus();
});
$("#console-input").keyup(function(e){
  if(e.keyCode == 13)
  {
    $("#console-text").append(">: "+$("#console-input").val() + "<br>");
    if ($("#console-input").val()=="hi") $("#console-text").append("Hello" + "<br>");
    else if ($("#console-input").val()=="grad") {
      if ($(".tabs").is(".gradient")) $(".tabs").removeClass("gradient");
      else $(".tabs").addClass("gradient");
    }
    else if ($("#console-input").val()=="img") {
      if ($(".tabs").is(".bg-image")) $(".tabs").removeClass("bg-image");
      else $(".tabs").addClass("bg-image");
    }
    else if ($("#console-input").val()=="close") {
      $("#console-text").text("");
      $("#console").fadeOut();
    }
    else $("#console-text").append("Unknown command" + "<br>");
    $("#console-input").val("");
  }
});

//CONTEXT MENU
/*
const remote = require('electron').remote;
const Menu = remote.Menu;
const MenuItem = remote.MenuItem;

var menu = new Menu();
menu.append(new MenuItem({ label: 'MenuItem1', click: function() { console.log('item 1 clicked'); } }));
menu.append(new MenuItem({ type: 'separator' }));
menu.append(new MenuItem({ label: 'MenuItem2', type: 'checkbox', checked: true }));

window.addEventListener('contextmenu', function (e) {
  e.preventDefault();
  menu.popup(remote.getCurrentWindow());
}, false);*/



//MAIN MENU
const remote = require('electron').remote;
const Menu = remote.Menu;
const MenuItem = remote.MenuItem;

const template = [
   {
      label: 'File',
      submenu: [
        {
          label: 'New File',
          accelerator: 'Ctrl+N',
          click () { newFile(); }
        },
        {
          label: 'Open File...',
          accelerator: 'Ctrl+O',
          click () { openFile(); }
        },
        {
          label: 'Save',
          accelerator: 'Ctrl+S',
          click () { saveFile(); }
        },
        {
          label: 'Save As...',
          accelerator: 'Shift+Ctrl+S',
          click () { saveFileAs(); }
        },
        {
          label: 'Close File',
          accelerator: 'Ctrl+W',
          click () { closeFile(); }
        },
        { role: 'quit' }
      ]
   },
   {
      label: 'Settings',
      submenu: [
        {
          label: 'Themes',
          submenu: [
            { label: 'default', click () { setTheme('default'); } },
            { label: '3024-day', click () { setTheme('3024-day'); } },
            { label: '3024-night', click () { setTheme('3024-night'); } },
            { label: 'abcdef', click () { setTheme('abcdef'); } },
            { label: 'ambiance', click () { setTheme('ambiance'); } },
            { label: 'base16-dark', click () { setTheme('base16-dark'); } },
            { label: 'base16-light', click () { setTheme('base16-light'); } },
            { label: 'bespin', click () { setTheme('bespin'); } },
            { label: 'blackboard', click () { setTheme('blackboard'); } },
            { label: 'cobalt', click () { setTheme('cobalt'); } },
            { label: 'colorforth', click () { setTheme('colorforth'); } },
            { label: 'dracula', click () { setTheme('dracula'); } },
            { label: 'duotone-dark',  click () { setTheme('duotone-dark'); } },
            { label: 'duotone-light', click () { setTheme('duotone-light'); } },
            { label: 'eclipse', click () { setTheme('eclipse'); } },
            { label: 'elegant', click () { setTheme('elegant'); } },
            { label: 'erlang-dark', click () { setTheme('erlang-dark'); } },
            { label: 'hopscotch', click () { setTheme('hopscotch'); } },
            { label: 'icecoder', click () { setTheme('icecoder'); } },
            { label: 'isotope', click () { setTheme('isotope'); } },
            { label: 'lesser-dark', click () { setTheme('lesser-dark'); } },
            { label: 'liquibyte', click () { setTheme('liquibyte'); } },
            { label: 'material', click () { setTheme('material'); } },
            { label: 'mbo', click () { setTheme('mbo'); } },
            { label: 'mdn-like', click () { setTheme('mdn-like'); } },
            { label: 'midnight', click () { setTheme('midnight'); } },
            { label: 'monokai', click () { setTheme('monokai'); } },
            { label: 'neat', click () { setTheme('neat'); } },
            { label: 'neo', click () { setTheme('neo'); } },
            { label: 'night', click () { setTheme('night'); } },
            { label: 'oceanic-next', click () { setTheme('oceanic-next'); } },
            { label: 'panda-syntax', click () { setTheme('panda-syntax'); } },
            { label: 'paraiso-dark', click () { setTheme('paraiso-dark'); } },
            { label: 'paraiso-light', click () { setTheme('paraiso-light'); } },
            { label: 'pastel-on-dark', click () { setTheme('pastel-on-dark'); } },
            { label: 'railscasts', click () { setTheme('railscasts'); } },
            { label: 'rubyblue', click () { setTheme('rubyblue'); } },
            { label: 'seti', click () { setTheme('seti'); } },
            { label: 'shadowfox', click () { setTheme('shadowfox'); } },
            { label: 'solarized dark', click () { setTheme('solarized dark'); } },
            { label: 'solarized light', click () { setTheme('solarized light'); } },
            { label: 'the-matrix', click () { setTheme('the-matrix'); } },
            { label: 'tomorrow-night-bright', click () { setTheme('tomorrow-night-bright'); } },
            { label: 'tomorrow-night-eighties', click () { setTheme('tomorrow-night-eighties'); } },
            { label: 'ttcn', click () { setTheme('ttcn'); } },
            { label: 'twilight', click () { setTheme('twilight'); } },
            { label: 'vibrant-ink', click () { setTheme('vibrant-ink'); } },
            { label: 'xq-dark', click () { setTheme('xq-dark'); } },
            { label: 'xq-light', click () { setTheme('xq-dark'); } },
            { label: 'yeti', click () { setTheme('yeti'); } },
            { label: 'zenburn', click () { setTheme('zenburn'); } }
          ]
        }
      ]
   },
   {
      label: 'Open',
      submenu: [
        {
          label: 'Open with default program',
          click () { runFile(); }
        }/*,
        {
          label: 'Open with Chrome',
          click () { runFile(); }
        },
        {
          label: 'Open with default Mozilla',
          click () { runFile(); }
        }*/
      ]
   },
   {
      label: 'About',
      submenu: [
        {
          label: 'Website',
          click () { shell.openExternal("https://source.dog/pte/"); }
        }/*,
        {
          label: 'Open with Chrome',
          click () { runFile(); }
        },
        {
          label: 'Open with default Mozilla',
          click () { runFile(); }
        }*/
      ]
   }
    /*
      label: 'Edit',
      submenu: [
         {
            role: 'undo'
         },
         {
            role: 'redo'
         },
         {
            type: 'separator'
         },
         {
            role: 'cut'
         },
         {
            role: 'copy'
         },
         {
            role: 'paste'
         }
      ]
   },
   
   {
      label: 'View',
      submenu: [
         {
            role: 'reload'
         },
         {
            role: 'toggledevtools'
         },
         {
            type: 'separator'
         },
         {
            role: 'resetzoom'
         },
         {
            role: 'zoomin'
         },
         {
            role: 'zoomout'
         },
         {
            type: 'separator'
         },
         {
            role: 'togglefullscreen'
         }
      ]
   },
   
   {
      role: 'window',
      submenu: [
         {
            role: 'minimize'
         },
         {
            role: 'close'
         }
      ]
   },
   
   {
      role: 'help',
      submenu: [
         {
            label: 'Learn More'
         }
      ]
   }*/
]

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);

//EXPERIMENTAL

//...

//UNHIDE LOADING SCREEN
$(document).ready(function() { $("#loader").hide(); });